function Layout({ children }) {
  return (
    <div className="container-fluid-without-header">
      <div className="container-lg px-0 px-md-2">{children}</div>
    </div>
  );
}

export default Layout;
