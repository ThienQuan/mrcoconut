import Logo from '~/assets/images/logo.png';
function Header() {
  return (
    <header id="header" className="container-fluid">
      <div className="row">
        <label className="mobile-menu-toggler">
          <div className="bar1"></div>
          <div className="bar2"></div>
          <div className="bar3"></div>
        </label>
        <div className="col-12 col-md-12 logo">
          <a className="active" aria-current="page" href="/dashboard">
            <img src={Logo} alt="Coconut" />
          </a>
        </div>
      </div>
    </header>
  );
}

export default Header;
