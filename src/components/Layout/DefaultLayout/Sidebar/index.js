import { useState } from 'react';
import { Modal } from 'react-bootstrap';

import Logo from '~/assets/images/logo.png';
import Dashboard from '~/assets/images/icons/icon-dashboard.png';
import Star from '~/assets/images/icons/icon-star.png';
import Transaction from '~/assets/images/icons/icon-mytransaction.png';
import Reward from '~/assets/images/icons/icon-reward.png';
import Invite from '~/assets/images/icons/icon-invite.png';
import Face from '~/assets/images/icons/icon-face.png';
import Pw from '~/assets/images/icons/icon-pw.png';
import Faq from '~/assets/images/icons/icon-faq.png';
import Contact from '~/assets/images/icons/icon-contact-us.png';
import Logout from '~/assets/images/icons/icon-logout.png';

function Sidebar() {
  const [showRef, setShowRef] = useState(false);
  const handleRefClose = () => setShowRef(false);
  const handleRefShow = () => setShowRef(true);
  return (
    <div className="border-end bg-white" id="sidebar-wrapper">
      <div className="sidebar-heading bg-white text-center">
        <a href="/dashboard">
          <img src={Logo} alt="" className="logo-brand" />
        </a>
      </div>
      <div className="list-group list-group-flush">
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="/dashboard"
        >
          <img src={Dashboard} alt="" className="pe-2" /> Dashboard
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="/rewards"
        >
          <img src={Star} alt="" className="pe-2" /> My Rewards
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="/transactions"
        >
          <img src={Transaction} alt="" className="pe-2" />
          My Transactions
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="/rewards-catalogue"
        >
          <img src={Reward} alt="" className="pe-2" />
          Rewards Catalogue
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="/invite-friends"
        >
          <img src={Invite} alt="" className="pe-2" />
          Invite Friends
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="/profile"
        >
          <img src={Face} alt="" className="pe-2" />
          Update Profile
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="/profile/change-password"
        >
          <img src={Pw} alt="" className="pe-2" />
          Change Password
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="#!"
        >
          <img src={Faq} alt="" className="pe-2" />
          FAQ
        </a>
        <a
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          href="#!"
        >
          <img src={Contact} alt="" className="pe-2" />
          Contact Us
        </a>
        <button
          className="list-group-item py-2 ps-5 border-0 d-flex align-items-center text-secondary fs-14px"
          onClick={handleRefShow}
        >
          <img src={Logout} alt="" className="pe-2" />
          Logout
        </button>

        <Modal show={showRef} onHide={handleRefClose} size="md" centered>
          <Modal.Header closeButton className="border-0"></Modal.Header>
          <Modal.Body className="text-secondary text-center">
            Are you sure you want to log out?
          </Modal.Body>
          <Modal.Footer className="border-0 flex-nowrap justify-content-center">
            <a href="/sign-in" className="btn mb-3 btn-primary border">
              Logout
            </a>
            <button
              type="button"
              className="btn mb-3 btnCancel border"
              onClick={handleRefClose}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>
      </div>
    </div>
  );
}

export default Sidebar;
