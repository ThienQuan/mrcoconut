import Header from '~/components/Layout/components/Header';
import Sidebar from './Sidebar';
import Footer from './Footer';

function DefaultLayout({ children }) {
  return (
    <div id="body">
      <Header />
      <div className="d-flex body-content">
        <Sidebar />
        <div id="page-content-wrapper" className="content">
          {children}
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default DefaultLayout;
