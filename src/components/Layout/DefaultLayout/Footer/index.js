function Footer() {
  return (
    <div className="container footer-link text-center pt-2 pb-3">
      <a href="#faq" className="link-footer">
        FAQ
      </a>
      <span className="link-footer"> | </span>
      <a href="#miss" className="link-footer">
        Terms &amp; Conditions
        <br className="d-block d-sm-block d-md-none d-xl-none d-lg-none" />
      </a>
      <span className="link-footer">
        {' '}
        ® 2022 Mr. Coconut. All Rights Reserved.
      </span>
    </div>
  );
}

export default Footer;
