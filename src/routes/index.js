// Layouts
import { /*HeaderOnly,*/ DefaultLayout, Layout } from '~/components/Layout';

// Pages
import SignIn from '~/pages/Signin';
import SignUp from '~/pages/Signup';
import ForgotPassword from '~/pages/ForgotPassword';
import Dashboard from '~/pages/Dashboard';
import Rewards from '~/pages/Rewards';
import Transactions from '~/pages/Transactions';
import RewardsCatalogue from '~/pages/RewardsCatalogue';
import InviteFriends from '~/pages/InviteFriends';
import Profile from '~/pages/Profile';
import ChangePassword from '~/pages/ChangePassword';
// import Home from '~/pages/Home';
// import Following from '~/pages/Following';
// import Upload from '~/pages/Upload';
// import Search from '~/pages/Search';

// Public routes
const publicRoutes = [
  { path: '/', component: SignIn, layout: Layout },
  { path: '/sign-up', component: SignUp, layout: Layout },
  { path: '/forgot-password', component: ForgotPassword, layout: Layout },
  { path: '/dashboard', component: Dashboard, layout: DefaultLayout },
  { path: '/rewards', component: Rewards, layout: DefaultLayout },
  { path: '/transactions', component: Transactions, layout: DefaultLayout },
  {
    path: '/rewards-catalogue',
    component: RewardsCatalogue,
    layout: DefaultLayout,
  },
  { path: '/invite-friends', component: InviteFriends, layout: DefaultLayout },
  { path: '/profile', component: Profile, layout: DefaultLayout },
  {
    path: '/profile/change-password',
    component: ChangePassword,
    layout: DefaultLayout,
  },
  // { path: '/home', component: Home, layout: DefaultLayout },
  // { path: '/following', component: Following, layout: DefaultLayout },
  // { path: '/upload', component: Upload, layout: HeaderOnly },
  // { path: '/search', component: Search, layout: null },
];

const privateRoutes = [];

export { publicRoutes, privateRoutes };
