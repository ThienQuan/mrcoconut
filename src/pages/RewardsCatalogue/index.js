// import { useState } from 'react';

function rewardsCatalogue() {
  return (
    <div className="site-content px-0 px-lg-auto mx-0">
      <div>
        <div className="fade-enter-done">
          <div id="rewards-catalogue" className="my-0">
            <div className="row box justify-content-center mx-0">
              <div className="ban-line col-12 d-none d-lg-block"></div>
              <div className="col-12 px-md-5 px-sm-2 py-md-3">
                <div className="row">
                  <div className="col mb-3">
                    <h5 className="form-title my-2">Rewards Catalogue</h5>
                    <hr className="rc-hr" />
                  </div>
                </div>
                <div className="row mx-1 mb-3">
                  <div className="col-12 nut-expire">
                    <div className="p-2">
                      <div className="card-text font-title mt-3 d-flex">
                        <p style={{ fontSize: '50px' }}>1</p>
                        <p style={{ paddingTop: '30px', paddingLeft: '15px', fontSize: '15px' }}>Nut</p>
                      </div>
                      <p className="card-text mt-3" style={{ fontSize: '15px' }}>
                        Expires on 06/09/2023
                      </p>
                    </div>
                  </div>
                </div>
                <div>
                  <div className="row mx-2">
                    <div className="item-redeem col-12 col-md-4 col-lg-3 col-sm-6 py-2 ps-2">
                      <div className="card-body">
                        <div className="col-icon images">
                          <img
                            src="https://edmsource.ascentismedia.com/MatrixResources/VoucherTypeImage/mrcoconut/F1UAA001.png"
                            alt="Complimentary free upsize from medium to large of any products (Baby Coconut)"
                            className="img-fluid w-100"
                          />
                        </div>
                        <div className="m-0 text-start mt-3">
                          <h5
                            className="text-des card-text d-flex flex-column justify-content-center fw-normal fs-6 mb-0"
                            style={{ height: '60px' }}
                          >
                            Complimentary Free Upsize from medium to large of any products
                          </h5>
                          <p className="card-valid mb-2">10 Nuts</p>
                          <p className="card-valid mb-2"></p>
                          <button className="btn btn-insufficicent mt-1 text-white btn-hover">
                            <h6 className="mb-0">Insufficient Points</h6>
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="item-redeem col-12 col-md-4 col-lg-3 col-sm-6 py-2 ps-2">
                      <div className="card-body">
                        <div className="col-icon images">
                          <img
                            src="https://edmsource.ascentismedia.com/MatrixResources/VoucherTypeImage/mrcoconut/F1TAA001.png"
                            alt="1 free topping of your choice for any drinks (Baby Coconut)"
                            className="img-fluid w-100"
                          />
                        </div>
                        <div className="m-0 text-start mt-3">
                          <h5
                            className="text-des card-text d-flex flex-column justify-content-center fw-normal fs-6 mb-0"
                            style={{ height: '60px' }}
                          >
                            1 X Free Topping of your choice of any drinks
                          </h5>
                          <p className="card-valid mb-2">10 Nuts</p>
                          <p className="card-valid mb-2"></p>
                          <button className="btn btn-insufficicent mt-1 text-white btn-hover">
                            <h6 className="mb-0">Insufficient Points</h6>
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="item-redeem col-12 col-md-4 col-lg-3 col-sm-6 py-2 ps-2">
                      <div className="card-body">
                        <div className="col-icon images">
                          <img
                            src="https://edmsource.ascentismedia.com/MatrixResources/VoucherTypeImage/mrcoconut/F1PAA001.png"
                            alt="1 free pudding (Baby Coconut)"
                            className="img-fluid w-100"
                          />
                        </div>
                        <div className="m-0 text-start mt-3">
                          <h5
                            className="text-des card-text d-flex flex-column justify-content-center fw-normal fs-6 mb-0"
                            style={{ height: '60px' }}
                          >
                            1 X Free Pudding
                          </h5>
                          <p className="card-valid mb-2">20 Nuts</p>
                          <p className="card-valid mb-2"></p>
                          <button className="btn btn-insufficicent mt-1 text-white btn-hover">
                            <h6 className="mb-0">Insufficient Points</h6>
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="item-redeem col-12 col-md-4 col-lg-3 col-sm-6 py-2 ps-2">
                      <div className="card-body">
                        <div className="col-icon images">
                          <img
                            src="https://edmsource.ascentismedia.com/MatrixResources/VoucherTypeImage/mrcoconut/F1DMA001.png"
                            alt="1 free medium drink of your choice (Baby Coconut)"
                            className="img-fluid w-100"
                          />
                        </div>
                        <div className="m-0 text-start mt-3">
                          <h5
                            className="text-des card-text d-flex flex-column justify-content-center fw-normal fs-6 mb-0"
                            style={{ height: '60px' }}
                          >
                            1 X Free Medium Drink of your choice
                          </h5>
                          <p className="card-valid mb-2">80 Nuts</p>
                          <p className="card-valid mb-2"></p>
                          <button className="btn btn-insufficicent mt-1 text-white btn-hover">
                            <h6 className="mb-0">Insufficient Points</h6>
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="item-redeem col-12 col-md-4 col-lg-3 col-sm-6 py-2 ps-2">
                      <div className="card-body">
                        <div className="col-icon images">
                          <img
                            src="https://edmsource.ascentismedia.com/MatrixResources/VoucherTypeImage/mrcoconut/F1PPA001.png"
                            alt="1 free jelly (Baby Coconut)"
                            className="img-fluid w-100"
                          />
                        </div>
                        <div className="m-0 text-start mt-3">
                          <h5
                            className="text-des card-text d-flex flex-column justify-content-center fw-normal fs-6 mb-0"
                            style={{ height: '60px' }}
                          >
                            1 X Free Jelly
                          </h5>
                          <p className="card-valid mb-2">22 Nuts</p>
                          <p className="card-valid mb-2"></p>
                          <button className="btn btn-insufficicent mt-1 text-white btn-hover">
                            <h6 className="mb-0">Insufficient Points</h6>
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="item-redeem col-12 col-md-4 col-lg-3 col-sm-6 py-2 ps-2">
                      <div className="card-body">
                        <div className="col-icon images">
                          <img
                            src="https://edmsource.ascentismedia.com/MatrixResources/VoucherTypeImage/mrcoconut/F1ITA001.png"
                            alt="1 free Ice-cream in cup with 3 Toppings of your choice (Baby Coconut)"
                            className="img-fluid w-100"
                          />
                        </div>
                        <div className="m-0 text-start mt-3">
                          <h5
                            className="text-des card-text d-flex flex-column justify-content-center fw-normal fs-6 mb-0"
                            style={{ height: '60px' }}
                          >
                            1 X Free Ice-Cream in Cup with 3 Toppings of your choice
                          </h5>
                          <p className="card-valid mb-2">50 Nuts</p>
                          <p className="card-valid mb-2"></p>
                          <button className="btn btn-insufficicent mt-1 text-white btn-hover">
                            <h6 className="mb-0">Insufficient Points</h6>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default rewardsCatalogue;
