/* eslint-disable eqeqeq */
import { useState } from 'react';
import Capcha from '~/assets/images/capcha.jpg';
import Capchanot from '~/assets/images/capcha-not.jpg';

function ForgotPassword() {
  const [validPhone, setValidPhone] = useState('');
  // const [requiredPass, setRequiredPass] = useState(true);
  const [scr, setScr] = useState(Capchanot);
  function onch(e) {
    // console.log(e);
    const { value, id } = e.target;
    if (id === 'phone') {
      if (value == '') {
        setValidPhone('is-invalid');
      } else {
        setValidPhone('is-valid');
      }
    }
  }
  function oncl(e) {
    const { value, id } = e.target;
    if (id === 'phone') {
      if (value == '') {
        setValidPhone('');
      } else {
        setValidPhone('is-valid');
      }
    }
  }
  return (
    <div className="d-flex justify-content-center pt-fgpass">
      <div className="row align-items-center container-fgpass">
        <div className="col mb-3 bg-body shadow-container-fgpass">
          <section className="content-fgpass">
            <h2 className="title-fgpass text-secondary mt-2 mb-0">Forgot Password</h2>
            <hr className="border border-primary1 border-2 opacity-75" />
            <p className="text-secondary fgpass-text">
              Enter the mobile number used to sign up for your
              <br /> account. We will send you an email with information on
              <br /> how to reset your password.
            </p>
            <div className="pt-1">
              <p className="text-secondary fgpass-text mb-0">Mobile Number *</p>
            </div>
            <form action="" className="row form-fgpass">
              <div className="input-group my-3">
                <span className="input-group-text" id="basic-addon1">
                  +84
                </span>
                <input
                  type="number"
                  className={'form-control p-08 ' + validPhone}
                  placeholder="Mobile No"
                  aria-label="Mobile No"
                  aria-describedby="basic-addon1"
                  id="phone"
                  onClick={oncl}
                  onChange={onch}
                  required
                />
              </div>
              <div className="input-group">
                <div className="capcha-gg">
                  <img className="pb-4 w-75 float-start w-capcha" src={scr} onClick={() => setScr(Capcha)} alt="" />
                </div>
              </div>
              <div className="col-8">
                <button type="submit" className="btn btn-primary1 mb-3 w-100 text-white btn-hover">
                  Sign in
                </button>
                <a href="/sign-in" className="btn mb-3 w-100 btn-hover btnCancel">
                  Cancel
                </a>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  );
}

export default ForgotPassword;
