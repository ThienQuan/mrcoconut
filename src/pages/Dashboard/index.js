import SuperCoconutFigure from '~/assets/images/super-coconut-figure.png';
function Dashboard() {
  return (
    <div className="site-content px-lg-0 mx-0">
      <div>
        <div>
          <div id="reward-list" className="my-0">
            <div class="ban-line col-12 d-none d-lg-block"></div>
            <div className="row box justify-content-center mx-lg-0">
              <div className="col-12 px-md-5 px-sm-2">
                <div className="row">
                  <h4 className="col py-3 form-title card-text">Welcome, Joe</h4>
                </div>
                <div className="row">
                  <div className="col-md-4 col-sm-12 my-1">
                    <div className="nut-expire p-2">
                      <p className="card-text font-title mt-2" style={{ fontSize: '20px' }}>
                        Mr.Coconut Loyalty
                      </p>
                      <div className="card-text font-title mt-3 d-flex">
                        <p style={{ fontSize: '20px' }}>1</p>
                        <p style={{ paddingTop: '15px', paddingLeft: '5px' }}>Nut</p>
                      </div>
                      <p className="card-text">Expires on 06/09/2023</p>
                    </div>
                  </div>
                  <div className="col-md-8 col-sm-12 mt-1 icon-super">
                    <div className="row">
                      <div className="col-md-8 col-sm-12 mt-1">
                        <h4 className="card-tier-code font-title mb-0">Baby Coconut</h4>
                        <hr className="userid" />
                        <p className="text-blue">Member ID: orhhn8352498</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col card-progress">
                        <p className="card-text text-super mt-1 mb-2 pb-2">
                          You're $800.00 away from becoming Super Coconut!
                        </p>
                        <div className="progress">
                          <div
                            className="progress-bar pt-1"
                            role="progressbar"
                            aria-valuenow="0"
                            aria-valuemin="0"
                            aria-valuemax="100"
                            style={{ width: '0%' }}
                          ></div>
                        </div>
                        <img className="img-fluid" src={SuperCoconutFigure} alt="" />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row images mb-3">
                  <div className="col-md-6 col-sm-12 mt-3">
                    <img
                      className="img-fluid"
                      src="https://crm3.ascentis.com.sg/MatrixResources/ContentImage/Mrcoconut/3c409ee3-926d-4c3b-9c81-382169c0a126.jpg"
                      alt=""
                    />
                  </div>
                  <div className="col-md-6 col-sm-12 mt-3">
                    <img
                      className="img-fluid"
                      src="https://crm3.ascentis.com.sg/MatrixResources/ContentImage/Mrcoconut/f60ffb97-e30f-4338-96d8-d05cc578fe7a.jpg"
                      alt=""
                    />
                  </div>
                </div>
                <div>
                  <h6 className="card-text form-title font-title mt-3 mb-3">Rewards you can claim</h6>
                  <div className="row">
                    <div className="col text-super card-text text-left py-2">
                      You have no rewards to claim currently.
                    </div>
                  </div>
                  <h6 className="card-text form-title font-title my-3">Rewards you can redeem</h6>
                  <div className="row">
                    <div className="col text-super card-text text-left py-2">You need more nuts to redeem rewards.</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
