import { useState } from 'react';

function Rewards() {
  const [addClassAvai, setAddClassAvai] = useState('tab-list-active');
  const [addClassRedee, setAddClassRedee] = useState('');
  const [addClassExp, setAddClassExp] = useState('');
  function handlechange(e) {
    const { id } = e.target;
    if (id === 'avai') {
      setAddClassAvai('tab-list-active');
      setAddClassRedee('');
      setAddClassExp('');
    } else if (id === 'redee') {
      setAddClassAvai('');
      setAddClassRedee('tab-list-active');
      setAddClassExp('');
    } else if (id === 'exp') {
      setAddClassAvai('');
      setAddClassRedee('');
      setAddClassExp('tab-list-active');
    }
  }
  return (
    <div className="site-content px-lg-0 mx-0">
      <div>
        <div className="fade-enter-done">
          <div className="my-0" id="my-rewards">
            <div className="row box justify-content-center pb-5 mx-0">
              <div className="ban-line col-12 d-none d-lg-block"></div>
              <div className="col-12 px-0 px-md-5 px-sm-2 py-md-3">
                <div className="row">
                  <div className="col mb-2">
                    <h5 className="form-title my-2">My Rewards</h5>
                    <hr className="rc-hr" />
                  </div>
                </div>
                <div className="col-12 col-md-12 col-sm-12 tab-panel">
                  <div className="row">
                    <div className="col-12 col-md-12 col-sm-12 tabs px-4">
                      <ol className="row tab-list">
                        <li
                          id="avai"
                          className={'col-4 col-md-4 col-sm-4 text-center tab-list-item ' + addClassAvai}
                          onClick={handlechange}
                        >
                          Available
                        </li>
                        <li
                          id="redee"
                          className={'col-4 col-md-4 col-sm-4 text-center tab-list-item ' + addClassRedee}
                          onClick={handlechange}
                        >
                          Redeemed
                        </li>
                        <li
                          id="exp"
                          className={'col-4 col-md-4 col-sm-4 text-center tab-list-item ' + addClassExp}
                          onClick={handlechange}
                        >
                          Expired
                        </li>
                      </ol>
                      <div className="row tab-content mt-3">
                        <div className="col-12 px-0">
                          <div className="row">
                            <div className="text-start py-2 px-3 table-col">You do not have available rewards</div>
                          </div>{' '}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Rewards;
