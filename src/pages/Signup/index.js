/* eslint-disable eqeqeq */
import { useState } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

import InfoPicture from '~/assets/images/icons/icon-i.svg';
import IconCoconut from '~/assets/images/icon-coconut-reg.png';
import { Modal } from 'react-bootstrap';

function ForgotPassword() {
  const [validPhone, setValidPhone] = useState('');
  const [showRef, setShowRef] = useState(false);
  const [showAgree, setShowAgree] = useState(false);
  const [date, setDate] = useState(null);
  const [checked, setChecked] = useState(true);
  function onch(e) {
    // console.log(e);
    const { value, id } = e.target;
    if (id === 'phone') {
      if (value == '') {
        setValidPhone('is-invalid');
      } else {
        setValidPhone('is-valid');
      }
    }
  }
  function oncl(e) {
    const { value, id } = e.target;
    if (id === 'phone') {
      if (value == '') {
        setValidPhone('');
      } else {
        setValidPhone('is-valid');
      }
    }
  }
  const handleRefClose = () => setShowRef(false);
  const handleRefShow = () => setShowRef(true);
  const handleAgreeClose = () => setShowAgree(false);
  const handleAgreeShow = () => setShowAgree(true);
  return (
    <div className="d-flex justify-content-center pt-fgpass">
      <div className="row align-items-center container-fgpass">
        <div className="col mb-3 bg-body shadow-container-fgpass">
          <section className="content-fgpass">
            <h2 className="title-fgpass text-secondary mt-2 mb-0">Register</h2>
            <hr className="border border-primary1 border-2 opacity-75" />
            <form action="" className="form-fgpass">
              <div className="mb-3">
                <label htmlFor="fname" className="form-label text-secondary">
                  First Name
                </label>
                <input type="text" className="form-control" id="fname" required />
              </div>
              <div className="mb-3">
                <label htmlFor="lname" className="form-label text-secondary">
                  Last Name
                </label>
                <input type="text" className="form-control" id="lname" required />
              </div>
              <div className="pt-1">
                <p className="text-secondary mb-0">Gender</p>
              </div>
              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="gender" id="male" value="0" />
                <label className="form-check-label" htmlFor="male">
                  Male
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="gender" id="female" value="1" />
                <label className="form-check-label" htmlFor="female">
                  Female
                </label>
              </div>
              <div className="mb-3">
                <label htmlFor="bday" className="form-label text-secondary">
                  Date of Birth (DD/MM/YYYY)
                </label>
                <DatePicker
                  dateFormat="dd/MM/yyyy"
                  placeholderText="DD/MM/YYYY"
                  type="date"
                  className="form-control"
                  id="bday"
                  selected={date}
                  onChange={(date) => {
                    setDate(date);
                    console.log(moment(date).format('DD/MM/YYYY'));
                  }}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="email" className="form-label text-secondary">
                  Email
                </label>
                <input type="email" className="form-control" id="email" required />
              </div>
              <div className="pt-1">
                <p className="text-secondary mb-0">Mobile Number</p>
              </div>
              <div className="input-group my-2">
                <span className="input-group-text" id="basic-addon1">
                  +84
                </span>
                <input
                  type="number"
                  className={'form-control ' + validPhone}
                  placeholder="Mobile No"
                  aria-label="Mobile No"
                  aria-describedby="basic-addon1"
                  id="phone"
                  onClick={oncl}
                  onChange={onch}
                  required
                />
                <button type="button" className="btn text-white btn-hover btnCancel btnGetOTP">
                  Get OTP
                </button>
              </div>
              <div className="mb-3">
                <label htmlFor="referral" className="form-label text-secondary">
                  Referral Code (if applicable)
                  <span className="icon-ref" onClick={handleRefShow}>
                    <img className="ms-1" src={InfoPicture} alt="" />
                  </span>
                </label>
                <input type="text" className="form-control" id="referral" />
                <Modal show={showRef} onHide={handleRefClose} size="lg" centered>
                  <Modal.Header closeButton className="border-0"></Modal.Header>
                  <Modal.Body className="text-secondary">
                    A referral code allow members to refer a friend into the membership programme.
                    <br />
                    Here’s how it works:
                    <br />
                    When an existing member refer a friend into the programme, the member will receive 2 nuts free. The
                    friend being referred will receive 1 nut free upon signing up.
                  </Modal.Body>
                  <Modal.Footer className="border-0">
                    <button type="button" className="btn mb-3 w-100 btnCancel border" onClick={handleRefClose}>
                      Cancel
                    </button>
                  </Modal.Footer>
                </Modal>
              </div>
              <div className="mb-3">
                <label htmlFor="password" className="form-label text-secondary">
                  Password
                </label>
                <input type="password" className="form-control" id="password" required />
              </div>
              <div className="mb-3">
                <label htmlFor="confirmPassword" className="form-label text-secondary">
                  Confirm Password
                </label>
                <input type="password" className="form-control" id="confirmPassword" required />
              </div>

              <div className="form-check">
                <input
                  className="form-check-input"
                  type="checkbox"
                  value=""
                  id="receive"
                  checked={checked}
                  onChange={() => setChecked(!checked)}
                />
                <label className="form-check-label text-secondary" htmlFor="receive">
                  Receive promotional emails from Mr.Coconut
                </label>
              </div>
              <div className="form-check">
                <input className="form-check-input" type="checkbox" value="" id="tncAgree" />
                <label className="form-check-label text-secondary" htmlFor="tncAgree">
                  I am signing up to become a Mr. Coconut Loyalty member and I accept the{' '}
                  <span className="text-decoration-underline" onClick={handleAgreeShow}>
                    general terms & conditions of use
                  </span>
                </label>
                <Modal show={showAgree} onHide={handleAgreeClose} size="lg" centered>
                  <Modal.Header closeButton className="border-0"></Modal.Header>
                  <Modal.Body className="">
                    <div>
                      <title></title>
                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Terms and Conditions
                          </strong>
                        </span>
                      </p>

                      <p></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Definition
                          </strong>
                        </span>
                      </p>

                      <p></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            “Application”
                          </strong>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            {' '}
                            - The signing up process completed and submitted by the applicant to officially become a
                            member of the Mr. Coconut Loyalty Programme.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            “Membership”
                          </strong>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            {' '}
                            - Mr. Coconut Loyalty Programme.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            “Transaction”
                          </strong>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            {' '}
                            - Transaction effected by the member at any Mr. Coconut outlets.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            “Participating Outlets”{' '}
                          </strong>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            - Mr Coconut’s outlet in Singapore that acknowledge the use of Mr. Coconut Loyalty Programme
                            benefits.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            “Nuts”
                          </strong>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            {' '}
                            - A token issued to member while purchasing an item.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            “You, your, member, account”
                          </strong>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            {' '}
                            - the user who applied for Mr. Coconut Loyalty Programme membership and/or whose member’s
                            account belongs to.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Application of Mr. Coconut Loyalty Programme Membership
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Valid through our members portal (
                          </span>
                          <a href="https://loyalty.mrcoconut.sg/" style={{ textDecorationLine: 'none' }}>
                            <span
                              style={{
                                backgroundColor: 'transparent',
                                color: 'rgb(17, 85, 204)',
                                fontFamily: 'Arial',
                                fontSize: '11pt',
                                fontVariantEastAsian: 'normal',
                                fontVariantNumeric: 'normal',
                                textDecorationLine: 'underline',
                                textDecorationSkipInk: 'none',
                                verticalAlign: 'baseline',
                                whiteSpace: 'pre-wrap',
                              }}
                            >
                              https://loyalty.mrcoconut.sg/
                            </span>
                          </a>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            ).
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Membership is free.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Membership Validity
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Membership privileges can only be enjoyed by the member. Sharing of account is strictly not
                            allowed.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Earning Nuts
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members could earn nuts when they purchase any item.&nbsp;(Except for Jewel Changi Airport
                            outlet)
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Amount Spent:&nbsp;
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            $0.10 = 0.1 nut
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members are entitled to gain rewards and special privileges in the form of earning ‘nuts’
                            in participating outlets, based on their current ‘nut’ count.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members would have to identify oneself by informing our staff at the point of billing /
                            seating to enjoy our membership benefits.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Redemption of Nuts
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members{' '}
                          </span>
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            MUST{' '}
                          </strong>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            be physically present to receive their rewards. For example, when a Baby member has redeemed
                            20 nuts in their account, they would have to go to any of our participating outlets (except
                            Jewel Changi Airport) to receive their rewards. Date of birth or mobile numbers will be
                            requested for further verification purposes.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span
                          style={{
                            fontFamily: 'Arial, Helvetica, sans-serif',
                          }}
                        >
                          <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                            <span
                              style={{
                                backgroundColor: 'transparent',
                                color: 'rgb(0, 0, 0,)',
                                fontSize: '11pt',
                                fontVariantEastAsian: 'normal',
                                fontVariantNumeric: 'normal',
                                verticalAlign: 'baseline',
                                whiteSpace: 'pre-wrap',
                              }}
                            >
                              -’Nuts’ cannot be converted for cash or exchange for Mr. Coconut vouchers.
                            </span>
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span
                          style={{
                            fontSize: '14px',
                          }}
                        >
                          <span
                            style={{
                              fontFamily: 'Arial, Helvetica, sans-serif',
                            }}
                          >
                            -We do not provide membership points when ordering through third party delivery platforms.
                            (such as Grabfood/Deliveroo and mall&nbsp;rewards programme/points/vouchers/giftcards)
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Exclusive Privileges
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Members will be able to enjoy special privileges on their birthday month.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Expiry of Membership and Renewal
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -All unused ‘nuts’ will expire at the end of one year. For example, if you were to become a
                            member on 1st November 2019, any unused ‘nuts’ will expire on 31st October 2020.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Termination and Cancellation
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members may, at any time, terminate the membership.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -The account will be terminated immediately in the event of any breach of the T&amp;C.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -We reserve the rights to terminate the membership if found to be shared by his/her family
                            and friends.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Upon termination of the account, the member shall not attempt to use the account.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Upon the termination or cancellation of the account, all “nuts” will be forfeited and
                            member would not be able to claim from us.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span
                          style={{
                            fontSize: '20px',
                          }}
                        >
                          <span
                            style={{
                              fontFamily: 'Arial, Helvetica, sans-serif',
                            }}
                          >
                            <strong>Vouchers</strong>
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span
                          style={{
                            fontFamily: 'Arial, Helvetica, sans-serif',
                          }}
                        >
                          -You are only allowed to redeem 1 voucher per transaction only. (2 or more vouchers cannot be
                          used in a single transaction)
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Communication
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -After signing up as a member, you have given consent that we shall be entitled to disclose
                            personal particulars to the cooperatives and organizations that is related to us.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members have given consent to allow us to use their personal particulars in order to
                            facilitate membership benefits and entitlement.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members will have the responsiblility in ensuring that all email notifications updates are
                            not sent into their junk mail. Any conflicts are not negotiable if the emails are sent out
                            to the members.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Confidentiality of Account Details
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Members are required to keep their membership information and details confidential at all
                            times.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}></p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <strong
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '15pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              fontWeight: 700,
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            Discretion
                          </strong>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -The membership is the sole property of Mr. Coconut.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -If the rewards are unlawfully redeemed, the member will need to refund us the value of the
                            Rewards without causing any dispute among both parties.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -Any debate on the rewards shall be notified to us within 30 days from the date when the
                            dispute took place.
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -We reserve any rights to alter or amend any privilege or condition without prior notice.
                            All the terms and conditions of the rewards will be updated on our main website at&nbsp; (
                          </span>
                          <a href="https://mrcoconut.sg/" style={{ textDecorationLine: 'none' }}>
                            <span
                              style={{
                                backgroundColor: 'transparent',
                                color: 'rgb(17, 85, 204)',
                                fontFamily: 'Arial',
                                fontSize: '11pt',
                                fontVariantEastAsian: 'normal',
                                fontVariantNumeric: 'normal',
                                textDecorationLine: 'underline',
                                textDecorationSkipInk: 'none',
                                verticalAlign: 'baseline',
                                whiteSpace: 'pre-wrap',
                              }}
                            >
                              https://www.mrcoconut.sg/
                            </span>
                          </a>
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            ).
                          </span>
                        </span>
                      </p>

                      <p dir="ltr" style={{ lineHeight: '1.38', marginBottom: '0pt', marginTop: '0pt' }}>
                        <span id="docs-internal-guid-b9329425-7fff-d80f-5478-189af2f97205">
                          <span
                            style={{
                              backgroundColor: 'transparent',
                              color: 'rgb(0, 0, 0,)',
                              fontFamily: 'Arial',
                              fontSize: '11pt',
                              fontVariantEastAsian: 'normal',
                              fontVariantNumeric: 'normal',
                              verticalAlign: 'baseline',
                              whiteSpace: 'pre-wrap',
                            }}
                          >
                            -We reserve the right to suspend or terminate your membership should any of the conditions
                            and privileges are being abused.
                          </span>
                        </span>
                      </p>
                    </div>
                  </Modal.Body>
                  <Modal.Footer className="border-0">
                    <button
                      type="button"
                      className="btn mb-3 w-100 btnCancel border text-gray"
                      onClick={handleAgreeClose}
                    >
                      Cancel
                    </button>
                  </Modal.Footer>
                </Modal>
              </div>
              <div className="row mt-4">
                <div className="col-8">
                  <button type="submit" className="btn btn-primary1 mb-3 w-100 text-white btn-hover">
                    Register
                  </button>
                  <a href="/sign-in" className="">
                    Back To Login
                  </a>
                </div>
                <div className="col-4">
                  <img src={IconCoconut} alt="" />
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  );
}

export default ForgotPassword;
