function Transactions() {
  return (
    <div className="site-content">
      <div>
        <div className="fade-enter-done">
          <div id="my-transactions" className="my-0">
            <div className="row box justify-content-left text-left pb-5 mx-lg-0">
              <div className="ban-line col-12 d-none d-lg-block"></div>
              <div className="col-sm-12 col-xs-12 col-md-12 col-xl-9 col-lg-9 px-md-5 px-sm-2 py-md-3">
                <div className="row">
                  <h5 className="col-12 my-2 font-weight-normal card-text form-title">
                    My Transactions
                    <hr className="rc-hr" />
                  </h5>
                </div>
                <div>
                  <div className="in-line col-12 ">
                    <div className="row py-3 mx-0">
                      <div className="col-3 trs-detail">
                        <h6 className="date font-title text-secondary">07/09/2022</h6>
                        <h6 className="sub-info">17:15:52</h6>
                      </div>
                      <div className="col-7 trs-detail">
                        <h6 className="trans font-title text-secondary">Mr. Coconut @HQ</h6>
                        <div className="">
                          <h6 className="sub-info">Ref. AR637981677526022750</h6>
                        </div>
                      </div>
                      <div className="col-1 points text-end font-title ms-xs-0 ms-sm-1 ms-md-1 text-primary1">
                        <h6>+1</h6>
                        <h6>Nuts</h6>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-12 text-center">
                  <ul className="pagination">
                    {/* <li className="disabled">
                      <a className="" href="#miss" title="Go to first page"></a>
                    </li> */}
                    <li className="disabled">
                      <a className="" href="#miss" title="Go to previous page">
                        ⟨
                      </a>
                    </li>
                    <li className="active">
                      <a className="undefined" href="#miss" title="Go to page number 1">
                        1
                      </a>
                    </li>
                    <li className="disabled">
                      <a className="" href="#miss" title="Go to next page">
                        ⟩
                      </a>
                    </li>
                    {/* <li className="disabled">
                      <a className="" href="#miss" title="Go to last page"></a>
                    </li> */}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Transactions;
