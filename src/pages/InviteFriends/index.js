// import { useState } from 'react';
import Icon1 from '~/assets/images/icon1.svg';
import Icon2 from '~/assets/images/icon2.svg';
import Icon3 from '~/assets/images/icon3.svg';

function inviteFriends() {
  return (
    <div className="site-content px-lg-0 mx-0">
      <div>
        <div className="fade-enter-done">
          <div id="invite-friends" className="my-0">
            <div className="row box justify-content-center pb-5 mx-lg-0">
              <div className="ban-line col-12 d-none d-lg-block"></div>
              <div className="col-12 px-md-5 px-sm-2 py-md-3">
                <div className="row">
                  <h4 className="col my-2 font-weight-normal card-text form-title">
                    Invite Friends
                    <hr className="rc-hr" />
                  </h4>
                </div>
                <p className="title card-text font-title mt-2">
                  Invite your friends to join Mr. Coconut Loyalty and get rewarded with 2 Nuts for every friend that
                  signs up!
                </p>
                <div className="row">
                  <div className="col-sm-12 col-md-6">
                    <div className="row justify-content-center">
                      <div className="col-4 card-text text-center p-3">
                        <img className="py-2" src={Icon1} alt="" />
                        <h5 className="text-number form-title">1</h5>
                        <p>Copy your referal code and share to your friends</p>
                      </div>
                      <div className="col-4 card-text text-center p-3">
                        <img className="py-2" src={Icon2} alt="" />
                        <h5 className="text-number form-title">2</h5>
                        <p>Your friend signs up for an account and is verified</p>
                      </div>
                      <div className="col-4 card-text text-center p-3">
                        <img className="py-2" src={Icon3} alt="" />
                        <h5 className="text-number form-title">3</h5>
                        <p>You get 2 Nuts and your friend gets 1 Nut</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="pt-3 mb-2">
                  <p className="card-text">Your referral code</p>
                </div>
                <div className="row pb-3">
                  <div className="col-sm-12 col-md-7">
                    <div className="row">
                      <div className="invite-code col-8 card-text text-start">
                        <input id="copyCode" type="text" name="code" disabled="" defaultValue="rcocBfhE" />
                      </div>
                      <div className="copy col-4 card-text text-start">
                        <button className="btn btn-primary">
                          <h6 className="mb-0">Copy</h6>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="pt-3">
                  <p className="card-text">Share on social</p>
                </div>
                <div
                  className="addthis_inline_share_toolbox"
                  data-url="https://loyalty.mrcoconut.sg/sign-up/rcocBfhE"
                  data-title="Hi! Sign up as a Mr. Coconut member now with my referral code rcocBfhE or use the below link to get 1 bonus nut on signup."
                ></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default inviteFriends;
