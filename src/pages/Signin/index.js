/* eslint-disable eqeqeq */
import { useState } from 'react';
import Logo from '~/assets/images/logo.png';
import Drink from '~/assets/images/icon-free-drink.png';
import Birthday from '~/assets/images/icon-birthday.png';
import Promotion from '~/assets/images/icon-promotion.png';
import Capcha from '~/assets/images/capcha.jpg';
import Capchanot from '~/assets/images/capcha-not.jpg';

function SignIn() {
  const [validPhone, setValidPhone] = useState('');
  const [validPass, setValidPass] = useState('');
  const [requiredPass, setRequiredPass] = useState(true);
  const [scr, setScr] = useState(Capchanot);
  function onch(e) {
    // console.log(e);
    const { value, id } = e.target;
    if (id === 'phone') {
      if (value == '') {
        setValidPhone('is-invalid');
      } else {
        setValidPhone('is-valid');
      }
    } else {
      if (value == '') {
        setValidPass('is-invalid');
        setRequiredPass(false);
      } else {
        setValidPass('is-valid');
        setRequiredPass(true);
      }
    }
  }
  function oncl(e) {
    const { value, id } = e.target;
    if (id === 'phone') {
      if (value == '') {
        setValidPhone('');
      } else {
        setValidPhone('is-valid');
      }
    } else {
      if (value == '') {
        setValidPass('');
      } else {
        setValidPass('is-valid');
        setRequiredPass(true);
      }
    }
  }
  return (
    <div className="d-flex justify-content-center pt-signin">
      <div className="row align-items-center container-signin">
        <div className="col p-3 mb-3 bg-body shadow-container-signin">
          <section className="logo-welcome">
            <div className="d-flex justify-content-center">
              <a href="/" className="signin-href">
                <img src={Logo} alt="" className="logo-sign-in mt-4" />
              </a>
            </div>
            <div className="d-flex justify-content-center text-center">
              <a href="/" className="text-decoration-none text-dark signin-welcome">
                <h1 className="my-4 font-title text-title text-secondary">Welcome</h1>
              </a>
            </div>
          </section>
          <section className="form-signin">
            <div className="container text-center">
              <div className="row">
                <div className="col-lg-6 py5 border-end noborder-e border-bottom noborder-lg-b">
                  <h5 className="text-title text-primary">Sign in to Mr.Coconut Loyalty</h5>
                  <form action="" className="px-lg-4">
                    <div className="input-group my-3">
                      <span className="input-group-text" id="basic-addon1">
                        +84
                      </span>
                      <input
                        type="number"
                        className={'form-control p-08 ' + validPhone}
                        placeholder="Mobile No"
                        aria-label="Mobile No"
                        aria-describedby="basic-addon1"
                        id="phone"
                        onClick={oncl}
                        onChange={onch}
                        required
                      />
                    </div>
                    <div className="input-group my-3">
                      <input
                        type="password"
                        className={'form-control p-08 ' + validPass}
                        id="inputPassword"
                        placeholder="Password"
                        onClick={oncl}
                        onChange={onch}
                        required
                      />
                    </div>
                    {requiredPass == false ? (
                      <div className="input-group my-3">
                        <div className="text-danger">Password is required</div>
                      </div>
                    ) : (
                      <></>
                    )}
                    <div className="input-group my-3">
                      <div className="capcha-gg">
                        <img
                          className="pb-2 w-75 float-start w-capcha"
                          src={scr}
                          onClick={() => setScr(Capcha)}
                          alt=""
                        />
                      </div>
                    </div>
                    <div className="input-group my-3">
                      <button type="submit" className="btn btn-primary mb-3 w-100 btn-hover">
                        Sign in
                      </button>
                    </div>
                  </form>
                  <div className="col">
                    <div className="p-3">
                      <a href="/forgot-password" className="text-link">
                        Forgot your password
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 px-3 pb-5">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-12 text-center">
                        <h5 className="mb-2 text-title font-title text-center text-primary">Not a member yet?</h5>
                      </div>
                      <div className="col-12">
                        <p className="signin-text font-title text-align-center text-secondary bold fs-6">
                          <span>Join now! Membership is FREE, you'll receive 1 nut FREE upon signing up and...</span>
                        </p>
                      </div>
                      <div className="col-12">
                        <div className="row justify-content-center text-secondary">
                          <div className="col-4 signin-text p-1">
                            <img className="pb-2" src={Drink} alt="" />
                            <p>Earn free drinks and merchandise</p>
                          </div>
                          <div className="col-4 signin-text p-1">
                            <img className="pb-2" src={Birthday} alt="" />
                            <p>Get exclusive gifts on your birthday</p>
                          </div>
                          <div className="col-4 signin-text p-1">
                            <img className="pb-2" src={Promotion} alt="" />
                            <p>Be notified of promotions and events</p>
                          </div>
                        </div>
                      </div>
                      <div className="col-12 font-title text-center pt-3">
                        <a href="/sign-up" className="btn btn-primary mb-3 w-100 btn-hover">
                          Register Now
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default SignIn;
