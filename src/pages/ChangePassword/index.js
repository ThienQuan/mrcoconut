function changePassword() {
  return (
    <div className="site-content px-lg-0 mx-0">
      <div>
        <div className="fade-enter-done">
          <div id="change-password" className="my-0">
            <div className="row box justify-content-center pb-5 mx-0">
              <div className="ban-line col-12 d-none d-lg-block"></div>
              <div className="col-12 px-md-5 px-sm-2 py-md-3">
                <div className="row">
                  <h5 className="col my-2 form-title card-text">
                    Change Password
                    <hr className="rc-hr" />
                  </h5>
                </div>
                <div className="row">
                  <div className="col-sm-12 col-md-5">
                    <form name="form" className="my-3 row form">
                      <div className="col-12">
                        <div className="form-group mb-3">
                          <div className="textbox-container">
                            <label
                              data-required="true"
                              htmlFor="oldPassword"
                              className="textbox_label textbox_label-primary"
                            >
                              Current Password
                            </label>
                            <input
                              name="oldPassword"
                              id="oldPassword"
                              required=""
                              type="password"
                              className="textbox textbox-primary"
                              defaultValue=""
                            />
                          </div>
                        </div>
                        <div className="form-guide pt-2">
                          Password has to be minimum 8 alphanumeric characters
                        </div>
                        <div className="form-group mb-3">
                          <div className="textbox-container">
                            <label
                              data-required="true"
                              htmlFor="newPassword"
                              className="textbox_label textbox_label-primary"
                            >
                              New Password
                            </label>
                            <input
                              name="newPassword"
                              id="newPassword"
                              required=""
                              type="password"
                              className="textbox textbox-primary"
                              defaultValue=""
                            />
                          </div>
                        </div>
                        <div className="form-group mb-3">
                          <div className="textbox-container">
                            <label
                              data-required="true"
                              htmlFor="confirmNewPassword"
                              className="textbox_label textbox_label-primary"
                            >
                              Confirm New Password
                            </label>
                            <input
                              name="confirmNewPassword"
                              id="confirmNewPassword"
                              required=""
                              type="password"
                              className="textbox textbox-primary"
                              defaultValue=""
                            />
                          </div>
                        </div>
                        <div className="col-12 text-center">
                          <div className="row form-group px-2">
                            <div className="col-md-6 col-12 update pt-3">
                              <button
                                type="submit"
                                className="btn btn-primary btn-update mt-2"
                                disabled=""
                              >
                                Update
                              </button>
                            </div>
                            <div className="col-md-6 col-12 cancel pt-3">
                              <button
                                type="button"
                                className="btn btn-cancel mt-2"
                              >
                                Cancel
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default changePassword;
