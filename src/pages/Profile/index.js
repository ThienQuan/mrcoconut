import { useState } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Select from 'react-select';

import 'react-datepicker/dist/react-datepicker.css';

function Profile() {
  const [date, setDate] = useState(null);
  const [isSig, setIsSig] = useState(true);
  const [getPhone, setGetPhone] = useState({
    background: '#868484',
  });
  const getAllNation = require('~/assets/json/GetAllNationalities.json');
  const getNation = getAllNation.result.map((nation) => ({
    value: nation.systemCode,
    label: nation.name,
  }));
  const getAllOccupation = require('~/assets/json/GetOccupations.json');
  const getOccupation = getAllOccupation.result.map((occupation) => ({
    value: occupation.systemCode,
    label: occupation.name,
  }));
  const getIncomeGroups = require('~/assets/json/GetIncomeGroups.json');
  const incomeGroups = getIncomeGroups.result.map((incomeGroup) => ({
    value: incomeGroup.systemCode,
    label: incomeGroup.name,
  }));
  // console.log(getNation);

  const handleCountry = (event = []) => {
    const countryCode = event ? event.value : null;
    if (countryCode === 'SG') {
      setIsSig(true);
    } else {
      setIsSig(false);
    }
    console.log(countryCode);
  };
  const handlePhone = (e) => {
    const phone = e.target.value;
    if (phone.length > 7) {
      setGetPhone({ background: 'rgb(143, 190, 62)' });
    } else {
      setGetPhone({ background: '#868484' });
    }
  };
  const style = {
    control: (base) => ({
      ...base,
      width: '100%',
      height: '40px',
      border: '1px solid #b17c2a',
      color: '#b17c2a',
      borderRadius: '2px',
      fontWeight: '600',
      ':hover': {
        color: '#b17c2a',
        border: '1px solid #b17c2a',
      },
      ':active': {
        color: '#b17c2a',
        border: '1px solid #b17c2a',
      },
    }),
    option: (provided, { isFocused, isSelected }) => ({
      ...provided,
      // color: '#b17c2a',
      background: isFocused ? '#f1f1f1' : isSelected ? '#e5e5e5' : 'white',
    }),
    singleValue: (style) => ({
      ...style,
      color: '#b17c2a',
    }),
    dropdownIndicator: (style) => ({
      ...style,
      color: '#b17c2a',
      ':hover': {
        color: '#b17c2a',
      },
    }),
    clearIndicator: (style) => ({
      ...style,
      color: '#b17c2a',
      ':hover': {
        color: '#b17c2a',
      },
    }),
    indicatorSeparator: (style) => ({
      ...style,
      display: 'none',
    }),
  };

  return (
    <div className="site-content px-lg-0 mx-0">
      <div>
        <div className="fade-enter-done">
          <div className="my-0" id="my-profile">
            <div className="row box justify-content-center m-0">
              <div className="ban-line col-12 d-none d-lg-block"></div>
              <div className="col-12 px-md-5 px-sm-2 py-md-3">
                <div className="row">
                  <div className="col-12">
                    <h5 className="form-title font-title my-2">
                      Update Profile
                    </h5>
                    <hr className="rc-hr" />
                  </div>
                </div>
                <div>
                  <p
                    className="mt-2 px-3"
                    style={{ color: 'rgb(179, 209, 51)' }}
                  >
                    * Mandatory field
                  </p>
                </div>
                <form name="form" className="form">
                  <div className="row px-3">
                    <div className="col-lg-6 col-12">
                      <p className="text-green font-title">
                        Personal Information
                      </p>
                      <div className="form-group mb-3">
                        <div className="textbox-container">
                          <label
                            htmlFor="firstName"
                            className="textbox_label textbox_label-primary"
                          >
                            First Name *
                          </label>
                          <input
                            name="firstName"
                            id="firstName"
                            required
                            type="text"
                            className="textbox textbox-primary"
                            defaultValue="Joe"
                          />
                        </div>
                      </div>
                      <div className="form-group mb-3">
                        <div className="textbox-container">
                          <label
                            htmlFor="lastName"
                            className="textbox_label textbox_label-primary"
                          >
                            Last Name *
                          </label>
                          <input
                            name="lastName"
                            id="lastName"
                            required
                            type="text"
                            className="textbox textbox-primary"
                            defaultValue="Ph"
                          />
                        </div>
                      </div>
                      <label className="textbox_label textbox_label-primary">
                        Gender *
                      </label>
                      <div className="form-check mb-1 p-0">
                        <div className="form-check-inline d-block">
                          <input
                            className="form-check-input me-1 ms-0"
                            type="radio"
                            name="inlineRadioOptions"
                            id="inlineRadio1"
                            defaultValue="M"
                          />
                          <label
                            className="form-check-label me-3"
                            htmlFor="inlineRadio1"
                          >
                            Male
                          </label>
                          <input
                            className="form-check-input me-1 ms-0"
                            type="radio"
                            name="inlineRadioOptions"
                            id="inlineRadio2"
                            defaultValue="F"
                          />
                          <label
                            className="form-check-label"
                            htmlFor="inlineRadio2"
                          >
                            Female
                          </label>
                        </div>
                      </div>
                      <label className="textbox_label textbox_label-primary">
                        Date of Birth (DD/MM/YYYY) *
                      </label>
                      <div className="form-group mb-3 react-datepicker-component">
                        <DatePicker
                          dateFormat="dd/MM/yyyy"
                          placeholderText="20/12/1983"
                          type="date"
                          className="form-control react-datepicker-input"
                          id="bday"
                          selected={date}
                          onChange={(date) => {
                            setDate(date);
                            console.log(moment(date).format('DD/MM/YYYY'));
                          }}
                          disabled
                        />
                      </div>
                      <div className="form-group mb-3">
                        <div className="textbox-container">
                          <label
                            htmlFor="email"
                            className="textbox_label textbox_label-primary"
                          >
                            Email Address *
                          </label>
                          <input
                            name="email"
                            id="email"
                            placeholder="Email Address *"
                            required
                            type="email"
                            className="textbox textbox-primary"
                            defaultValue="joseph.pnc@gmail.com"
                          />
                        </div>
                      </div>
                      <label className="textbox_label textbox_label-primary">
                        Mobile Number *
                      </label>
                      <div className="row form-group mb-3">
                        <div className="phoneField col-lg-12 col-md-12 col-12">
                          <div className="form-group mob">
                            <div className="row">
                              <div className="country col-lg-3 col-md-3 col-3 pr-2">
                                <div className="form-group">
                                  <div className="textbox-container">
                                    <label
                                      htmlFor="country"
                                      className="textbox_label textbox_label-primary d-none"
                                    >
                                      undefined
                                    </label>
                                    <input
                                      name="country"
                                      id="country"
                                      placeholder="+65"
                                      disabled
                                      type="text"
                                      className="textbox textbox-primary"
                                      defaultValue="+65"
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="mobileNo col-lg-5 col-md-5 text-left col-5 px-0">
                                <div className="form-group">
                                  <div className="textbox-container">
                                    <label
                                      htmlFor="mobileNo"
                                      className="textbox_label textbox_label-primary d-none"
                                    >
                                      Mobile No{' '}
                                    </label>
                                    <input
                                      name="mobileNo"
                                      id="mobileNo"
                                      placeholder="Mobile No "
                                      required
                                      type="number"
                                      className="textbox textbox-primary"
                                      defaultValue="97767452"
                                      onChange={handlePhone}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="btn-getOtp col-lg-4 col-md-4 col-4 pl-2 py-0">
                                <div className="textbox-container text-right">
                                  <button
                                    className="w-100"
                                    type="button"
                                    style={getPhone}
                                  >
                                    Get OTP
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group mb-3">
                        <label
                          htmlFor="nationality"
                          className="textbox_label textbox_label-primary"
                        >
                          Nationality{' '}
                        </label>
                        <Select
                          options={getNation}
                          defaultValue={getNation[177]}
                          isClearable={true}
                          isSearchable={true}
                          styles={style}
                          placeholder={
                            <div className="Select-placeholder">
                              Select Nationality
                            </div>
                          }
                          onChange={handleCountry}
                        />
                      </div>
                      <div className="form-group mb-3">
                        <label
                          htmlFor="occupationCode"
                          className="textbox_label textbox_label-primary"
                        >
                          Occupation{' '}
                        </label>

                        <Select
                          options={getOccupation}
                          isClearable={true}
                          isSearchable={true}
                          styles={style}
                          placeholder={
                            <div className="Select-placeholder">
                              Select Occupation
                            </div>
                          }
                          onChange={handleCountry}
                        />
                      </div>
                      <div className="form-group mb-3">
                        <label
                          htmlFor="incomeGroupCode"
                          className="textbox_label textbox_label-primary"
                        >
                          Income Group{' '}
                        </label>
                        <Select
                          options={incomeGroups}
                          isClearable={true}
                          isSearchable={true}
                          styles={style}
                          placeholder={
                            <div className="Select-placeholder">
                              Select Income Group
                            </div>
                          }
                          onChange={handleCountry}
                        />
                      </div>
                      <p className="text-green font-title">
                        Subscription Options
                      </p>
                      <div className="tncEmail">
                        <div className="form-group text-left font-sub">
                          <div className="checkbox-container">
                            <input
                              name="tncEmail"
                              id="tncEmail"
                              type="checkbox"
                              className="checkbox me-1"
                              defaultValue="true"
                              defaultChecked=""
                            />
                            <label htmlFor="tncEmail">
                              <span className="txt textbox_label textbox_label-primary">
                                Receive promotional emails from Mr.Coconut
                              </span>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-12">
                      <p className="text-green font-title">
                        Address Information
                      </p>
                      <div className="form-group mb-3">
                        <label
                          htmlFor="country"
                          className="textbox_label textbox_label-primary"
                        >
                          Country{' '}
                        </label>
                        <Select
                          options={getNation}
                          defaultValue={getNation[177]}
                          isClearable={true}
                          isSearchable={true}
                          styles={style}
                          placeholder={
                            <div classNameName="Select-placeholder">
                              Select Country
                            </div>
                          }
                          onChange={handleCountry}
                        />
                      </div>
                      <div className="form-group mb-3">
                        <div className="textbox-container">
                          <label
                            htmlFor="postalCode"
                            className="textbox_label textbox_label-primary"
                          >
                            Postal Code
                          </label>
                          <input
                            name="postalCode"
                            id="postalCode"
                            placeholder="Postal Code"
                            required
                            type="text"
                            className="textbox textbox-primary"
                            defaultValue=""
                          />
                        </div>
                      </div>
                      {isSig === true ? (
                        <div>
                          <div className="form-group mb-3">
                            <div className="textbox-container">
                              <label
                                for="block"
                                className="textbox_label textbox_label-primary"
                              >
                                Block
                              </label>
                              <input
                                name="block"
                                id="block"
                                placeholder="Block"
                                required=""
                                type="text"
                                className="textbox textbox-primary"
                                value=""
                                data-dashlane-rid="e9156079c8190e7d"
                                data-form-type="other"
                              />
                            </div>
                          </div>
                          <div className="row level-unit pb-2">
                            <div className="col-6">
                              <div className="form-group mb-3">
                                <div className="textbox-container">
                                  <label
                                    for="level"
                                    className="textbox_label textbox_label-primary"
                                  >
                                    Level
                                  </label>
                                  <input
                                    name="level"
                                    id="level"
                                    placeholder="Level"
                                    required=""
                                    type="text"
                                    className="textbox textbox-primary"
                                    value=""
                                    data-dashlane-rid="e8a84f36aa7cd7d6"
                                    data-form-type="other"
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="card-text text-center p-0 w-0">
                              <hr className="line-center" />
                            </div>
                            <div className="col-6">
                              <div className="form-group mb-3">
                                <div className="textbox-container">
                                  <label
                                    for="unit"
                                    className="textbox_label textbox_label-primary"
                                  >
                                    Unit
                                  </label>
                                  <input
                                    name="unit"
                                    id="unit"
                                    placeholder="Unit"
                                    required=""
                                    type="text"
                                    className="textbox textbox-primary"
                                    value=""
                                    data-dashlane-rid="98b32382ee9cdbb8"
                                    data-form-type="address,extra"
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="form-group mb-3">
                            <div className="textbox-container">
                              <label
                                for="street"
                                className="textbox_label textbox_label-primary"
                              >
                                Street
                              </label>
                              <input
                                name="street"
                                id="street"
                                placeholder="Street"
                                required=""
                                type="text"
                                className="textbox textbox-primary"
                                value=""
                                data-dashlane-rid="433eca8ad6a0bf17"
                                data-kwimpalastatus="alive"
                                data-kwimpalaid="1665679345170-12"
                                data-form-type="address,street"
                              />
                            </div>
                          </div>
                          <div className="form-group mb-3">
                            <div className="textbox-container">
                              <label
                                for="building"
                                className="textbox_label textbox_label-primary"
                              >
                                Building
                              </label>
                              <input
                                name="building"
                                id="building"
                                placeholder="Building"
                                required=""
                                type="text"
                                className="textbox textbox-primary"
                                value=""
                                data-dashlane-rid="71d5fbf2811122bd"
                                data-form-type="other"
                              />
                            </div>
                          </div>
                        </div>
                      ) : (
                        <div className="row">
                          <div className="col-12 form-group mb-3">
                            <div className="form-group">
                              <div className="textbox-container">
                                <label
                                  htmlFor="address1"
                                  className="textbox_label textbox_label-primary"
                                >
                                  Address Line 1
                                </label>
                                <input
                                  name="address1"
                                  id="address1"
                                  placeholder="Address Line 1"
                                  required
                                  type="text"
                                  className="textbox textbox-primary"
                                  defaultValue=""
                                />
                              </div>
                            </div>
                          </div>
                          <div className="col-12 form-group mb-3">
                            <div className="form-group">
                              <div className="textbox-container">
                                <label
                                  htmlFor="address2"
                                  className="textbox_label textbox_label-primary"
                                >
                                  Address Line 2
                                </label>
                                <input
                                  name="address2"
                                  id="address2"
                                  placeholder="Address Line 2"
                                  required
                                  type="text"
                                  className="textbox textbox-primary"
                                  defaultValue=""
                                />
                              </div>
                            </div>
                          </div>
                          <div className="col-12 form-group mb-3">
                            <div className="form-group">
                              <div className="textbox-container">
                                <label
                                  htmlFor="address3"
                                  className="textbox_label textbox_label-primary"
                                >
                                  Address Line 3
                                </label>
                                <input
                                  name="address3"
                                  id="address3"
                                  placeholder="Address Line 3"
                                  required
                                  type="text"
                                  className="textbox textbox-primary"
                                  defaultValue=""
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="col-8 col-lg-4 pt-3 pb-5 ps-3">
                    <button
                      className="btn btn-primary font-title"
                      type="button"
                    >
                      Update Profile
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
